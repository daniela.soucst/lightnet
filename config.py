%%writefile lightnet/config.py

MODEL_PATH = "/content/gdrive/My Drive/cin/models"

DATASET_DIR = '/content/gdrive/My Drive/cin/data/BSDS500'
# DATASET_DIR = '/content/gdrive/My Drive/cin/data'
# DATASET_DIR = '/content/gdrive/My Drive/cin/data/Train400'

BATCH_SIZE = 128
LEARNING_RATE = 1e-2 #1-100
# LEARNING_RATE = 2e-4 #100-120
# LEARNING_RATE = 1e-1 #120

DATASET_TEST_DIR = '/content/gdrive/My Drive/cin/data/test'
RESULT_DIR = '/content/gdrive/My Drive/cin/results'

DARK_FACTOR = [0.1, 0.2, 0.3, 0.4,0.5]

USE_GLOBAL_ILLU = True
#USE_GLOBAL_ILLU = False

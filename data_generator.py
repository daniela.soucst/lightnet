%%writefile lightnet/data_generator.py
# -*- coding: utf-8 -*-

# =============================================================================
#  @article{zhang2017beyond,
#    title={Beyond a {Gaussian} denoiser: Residual learning of deep {CNN} for image denoising},
#    author={Zhang, Kai and Zuo, Wangmeng and Chen, Yunjin and Meng, Deyu and Zhang, Lei},
#    journal={IEEE Transactions on Image Processing},
#    year={2017},
#    volume={26}, 
#    number={7}, 
#    pages={3142-3155}, 
#  }
# by Kai Zhang (08/2018)
# cskaizhang@gmail.com
# https://github.com/cszn
# modified on the code from https://github.com/SaoYan/DnCNN-PyTorch
# =============================================================================

# no need to run this code separately

# from __future__ import print_function
import glob, os
import cv2
import numpy as np
# from multiprocessing import Pool
from torch.utils.data import Dataset
import torch
from config import *
import random
import os
import PIL
import scipy.io as sio

# patch_size, stride = 256, 1
patch_size, stride = 40, 10
# patch_size, stride = 16, 1
aug_times = 1
# scales = [1, 0.7]

scales = [1, 0.9, 0.8, 0.7]
# scales = [0.7]
batch_size = BATCH_SIZE

random.seed(0)


class DenoisingDataset(Dataset):
    """Dataset wrapping tensors.
    Arguments:
        xs (Tensor): clean image patches
        sigma: noise level, e.g., 25
    """

    def __init__(self, xs, illums):
        super(DenoisingDataset, self).__init__()
        self.xs = xs
        self.illums = illums
        # self.illum_factor = illum_factor

    def __getitem__(self, index):
        batch_x = self.xs[index]
        batch_y = batch_x * (self.illums[index])
        # batch_y = batch_x * illum_factor
        

      
        # batch_y = batch_x * (0.01)        

        return batch_x, batch_y, self.illums[index]

    def __len__(self):
        return self.xs.size(0)

class LightDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, illums, transform):
        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem
            real_illu (numpy array)
        """
        self.xs = root_dir # guarda as imagens propriamente ditas
        # self.img_names_list = img_names_list       
        self.illums =  illums
        self.transform = transform
        # self.real_rgb_list = real_illu

    def __len__(self):
        return self.xs.shape[0]

    def __getitem__(self, idx):
        original_image = self.xs[idx]

        # gd = self.illums[idx]
        gd = random.uniform(0., 0.5)
        dark_image = np.copy(original_image).astype(np.float32)       
        dark_image *= gd        
        dark_image = (dark_image).astype(np.uint8)       
          
       
        input_img = PIL.Image.fromarray(dark_image)
        output_img = PIL.Image.fromarray(original_image)
      
        sample = {'dark_image': input_img, 'clean_image': output_img}

        if self.transform:                      
          sample['dark_image'] = self.transform(sample['dark_image'])
          sample['clean_image'] = self.transform(sample['clean_image'])
        # print( 'size...', sample['dark_image'].shape, sample['clean_image'].shape)  
        return sample

def show(x, title=None, cbar=False, figsize=None):
    import matplotlib.pyplot as plt
    plt.figure(figsize=figsize)
    plt.imshow(x, interpolation='nearest', cmap='gray')
    if title:
        plt.title(title)
    if cbar:
        plt.colorbar()
    plt.show()


def data_aug(img, mode=0):
    # data augmentation
    if mode == 0:
        return img
    elif mode == 1:
        return np.flipud(img)
    elif mode == 2:
        return np.rot90(img)
    elif mode == 3:
        return np.flipud(np.rot90(img))
    elif mode == 4:
        return np.rot90(img, k=2)
    elif mode == 5:
        return np.flipud(np.rot90(img, k=2))
    elif mode == 6:
        return np.rot90(img, k=3)
    elif mode == 7:
        return np.flipud(np.rot90(img, k=3))


def gen_patches(file_name):
    # get multiscale patches from a single image
    img = cv2.imread(file_name)  # 3 ch    
    # img = cv2.imread(file_name, 0)  # BGR image    
    
    if (img is not None):
        
        img = cv2.resize(img, (180, 180))  # adj dims       
        gd = random.uniform(0.1, 0.5) #illuminacao
        # h, w = img.shape 
        h, w, ch = img.shape # 3 ch 
        
        patches = []
        illums = []
        for s in scales:
            h_scaled, w_scaled = int(h*s), int(w*s)
            img_scaled = cv2.resize(img, (h_scaled, w_scaled), interpolation=cv2.INTER_CUBIC)
            # extract patches
            for i in range(0, h_scaled-patch_size+1, stride):
                for j in range(0, w_scaled-patch_size+1, stride):
                    x = img_scaled[i:i+patch_size, j:j+patch_size, :] # 3 ch 
                    for k in range(0, aug_times):
                        x_aug = data_aug(x, mode=np.random.randint(0, 8))                       
                        patches.append(x_aug)                    
                        # illums.append(gd)
                        illums.append(random.uniform(0.1, 0.5))
        
        return patches, illums
    else:
      print('Image nao encontrada:', file_name)
      return [], []

def datagenerator(data_dir='data/Train400', verbose=False, batch_size=50):
    # generate clean patches from a dataset
    file_list = glob.glob(data_dir+'/*.jpg')  # get name list of all .jpg files
    # file_list = glob.glob(data_dir+'/*.*')  # 291 base

    # initrialize
    data = []
    data_illu = []
    # generate patches   
    print('data generatooor: ', len(file_list),data_dir) 
    for i in range(len(file_list)):
    # for i in range(400):
        
        
        patches, illums = gen_patches(file_list[i]) 
        # print('cada img gera %d patches' % len(patches))
        if(len(patches)!= 0):         
          for i in range(len(patches)):
            data.append(patches[i])
            data_illu.append(illums[i])
        else:
           print('Nao achou ', file_list[i])

        if verbose:
            print(str(i+1) + '/' + str(len(file_list)) + ' is done ^_^')
    print('shape data', data[0].shape)
    data = np.array(data, dtype=np.uint8)
    # data = np.array(data, dtype='uint8')    
    data_illu = np.array(data_illu)
    # data = np.expand_dims(data, axis=3)
    # print('data tamanho::: ', data.shape)
    discard_n = len(data)-len(data)//batch_size*batch_size  # because of batch namalization
    data = np.delete(data, range(discard_n), axis=0)
    data_illu = np.delete(data_illu, range(discard_n), axis=0)
    print('^_^-training data finished-^_^')
    
    return data,  data_illu


if __name__ == '__main__': 

    data,illums = datagenerator(data_dir=DATASET_DIR+'/original', batch_size=128)
    print('salvando dados...')
    sio.savemat(os.path.join(DATASET_DIR,'xs.mat'),{'data':data, 'illums': illums})
    print('end \o')

    # arq = sio.loadmat(os.path.join(DATASET_DIR,'xs.mat'))['data']
    # arq2 = sio.loadmat(os.path.join(DATASET_DIR,'xs.mat'))['illums']
    # print(arq.shape, type(arq))
    # print(arq2.shape, type(arq2))

#    print('Shape of result = ' + str(res.shape))
#    print('Saving data...')
#    if not os.path.exists(save_dir):
#            os.mkdir(save_dir)
#    np.save(save_dir+'clean_patches.npy', res)
#    print('Done.')       

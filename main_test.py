%%writefile lightnet/main_test.py
# -*- coding: utf-8 -*-

# =============================================================================
#  @article{zhang2017beyond,
#    title={Beyond a {Gaussian} denoiser: Residual learning of deep {CNN} for image denoising},
#    author={Zhang, Kai and Zuo, Wangmeng and Chen, Yunjin and Meng, Deyu and Zhang, Lei},
#    journal={IEEE Transactions on Image Processing},
#    year={2017},
#    volume={26}, 
#    number={7}, 
#    pages={3142-3155}, 
#  }
# by Kai Zhang (08/2018)
# cskaizhang@gmail.com
# https://github.com/cszn
# modified on the code from https://github.com/SaoYan/DnCNN-PyTorch
# =============================================================================

# run this to test the model

import argparse
import os, time, datetime
# import PIL.Image as Image
import numpy as np
import cv2
import torch.nn as nn
import torch.nn.init as init
import torch
from skimage.measure import compare_psnr, compare_ssim
from skimage.io import imread, imsave
from data_generator import DenoisingDataset, LightDataset
import random
from config import *
from sklearn.metrics import mean_squared_error
# from sewar.full_ref import vifp
random.seed(0)  # for reproducibility
import utils
import video_quality.vifp as vif

"""set_names : 'LIME', 'NPE', 'DICM', 'MEF', 'VV' """

# model_name ='LINET_MEAN_D17_lr0.01'
# model_name = 'WIIEN_SSIM_ch1'
#model_name = 'WIIEN_SSIM_red'
model_name = 'WIIEN_SSIM'
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--set_dir', default=DATASET_TEST_DIR+'/bases_reais', type=str, help='directory of test dataset')
    parser.add_argument('--set_names', default=['NPE', 'DICM', 'MEF', 'VV'], help='directory of test dataset')
    # parser.add_argument('--sigma', default=25, type=int, help='noise level')
    # parser.add_argument('--model_dir', default=os.path.join(MODEL_PATH, 'WIIEN_SSIM'), help='directory of the model')
    # parser.add_argument('--model_dir', default=os.path.join(MODEL_PATH, 'LINET_MEAN_D17_lr0.01'), help='directory of the model')
    parser.add_argument('--model_dir', default=os.path.join(MODEL_PATH, model_name), help='directory of the model')

    parser.add_argument('--model_name', default='model_200.pth', type=str, help='the model name')
    parser.add_argument('--result_dir', default=RESULT_DIR, type=str, help='directory of test dataset')
    parser.add_argument('--save_result', default=1, type=int, help='save the denoised image, 1 or 0')
    return parser.parse_args()

# def log(*args, **kwargs):
#      print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S:"), *args, **kwargs)


def save_result(result, path):
    path = path if path.find('.') != -1 else path+'.png'
    ext = os.path.splitext(path)[-1]
    if ext in ('.txt', '.dlm'):
        f = open(path, "a+")
        # if f is not None:
        f.close()
        np.savetxt(path, result, fmt='%2.4f')
    else:
        # imsave(path, np.clip(result, 0, 1))
        e_img = (np.clip(result, 0, 1)*255).astype(np.uint8)        
        cv2.imwrite(path, cv2.cvtColor(e_img,  cv2.COLOR_BGR2RGB))
        

def show(x, title=None, cbar=False, figsize=None):
    import matplotlib.pyplot as plt
    plt.figure(figsize=figsize)
    plt.imshow(x, interpolation='nearest', cmap='gray')
    if title:
        plt.title(title)
    if cbar:
        plt.colorbar()
    plt.show()



if __name__ == '__main__':

    args = parse_args()
    
    if not os.path.exists(os.path.join(args.model_dir, args.model_name)):
        print('Modelo nao encontrado!')
        quit()
    else:
        # model.load_state_dict(torch.load(os.path.join(args.model_dir, args.model_name)))
        model = torch.load(os.path.join(args.model_dir, args.model_name))
        print('load trained model')

#    params = model.state_dict()
#    print(params.values())
#    print(params.keys())
#
#    for key, value in params.items():
#        print(key)    # parameter name
#    print(params['dncnn.12.running_mean'])
#    print(model.state_dict())

    model.eval()  # evaluation mode
#    model.train()

    if torch.cuda.is_available():
        model = model.cuda()

    if not os.path.exists(args.result_dir):
        os.mkdir(args.result_dir)
    
    if not os.path.exists(os.path.join(args.result_dir, model_name)):
        os.mkdir(os.path.join(args.result_dir, model_name))

    for set_cur in args.set_names:


        if not os.path.exists(os.path.join(args.result_dir, model_name, set_cur)) and USE_GLOBAL_ILLU:
            os.mkdir(os.path.join(args.result_dir, model_name, set_cur))

        if not os.path.exists(os.path.join(args.result_dir, model_name, set_cur+'_local')) and not USE_GLOBAL_ILLU :
            print('Criando dir ',os.path.join(args.result_dir, model_name, set_cur+'_local') )
            os.mkdir(os.path.join(args.result_dir, model_name, set_cur+'_local'))
       
        psnrs = []
        ssims = []
        vifps = []
        file_list = os.listdir(os.path.join(args.set_dir, set_cur))
        # for im in os.listdir(os.path.join(args.set_dir, set_cur)):
        for k in range(1, len(file_list)+1):
            #im = str(k)+'.bmp'
            im = str(k)+'.png'
            
            if im.endswith(".JPG") or im.endswith(".bmp") or im.endswith(".png"):
                
                 # VIF : input: enhanced image output: deg. image
                # x = np.array(imread(os.path.join(args.set_dir, 'kodak', im)), dtype=np.float32)/255.0
                
                y = np.array(imread(os.path.join(args.set_dir, set_cur, im)), dtype=np.float32)/255.0
                
                y = cv2.resize(y,(600,600))
                y_ = torch.from_numpy(np.reshape(y,(1,y.shape[2],y.shape[0], y.shape[1])))
                # print('y_shape: ', y_.shape)
                torch.cuda.synchronize()
                start_time = time.time()
                y_ = y_.cuda()
                x_,_ = model(y_)  # inference
                x_ = x_.view(y.shape[0], y.shape[1], 3)
                x_ = x_.cpu()                
                x_ = x_.detach().numpy().astype(np.float32)
                torch.cuda.synchronize()
                elapsed_time = time.time() - start_time

               
                vifp_x_ = vif.vifp_mscale(x_, y)
                print("imagem: %s VIF = %f" %(im, vifp_x_))
                if args.save_result:
                    name, ext = os.path.splitext(im)
                    # show(np.hstack((y, x_)))  # show the image
                    #print(os.path.join(args.result_dir, set_cur, im))
                    save_result(x_, path=os.path.join(args.result_dir, model_name, set_cur, im))  # save the denoised image
                    save_result(y, path=os.path.join(args.result_dir, model_name, set_cur, im[:-4]+'_entrada.bmp'))  # save the denoised image
                    
                    del x_  
                    del y
                    
                    torch.cuda.empty_cache() 
        #         psnrs.append(psnr_x_)
                # ssims.append(ssim_x_)
                vifps.append(vifp_x_)
        # psnr_avg = np.mean(psnrs)
        # ssim_avg = np.mean(ssims)
        vifp_avg = np.mean(vifps)
        std = np.std(vifps)
        # psnrs.append(psnr_avg)
        # ssims.append(ssim_avg)
        vifps.append(vifp_avg)
        if args.save_result:
            save_result(np.hstack((vifp_avg, std)), path=os.path.join(args.result_dir, model_name, set_cur, 'results.txt'))
        print('Datset: %s \n  VIFP = %f, Desvio = %f '%(set_cur, vifp_avg, std))

